#!/usr/bin/env racket
#lang racket
(require pict pict-abbrevs)
(require "../common/picts-with-Japanese.rkt")

(define ccla-vctr (current-command-line-arguments))
(define infile
  (if (< 0 (vector-length ccla-vctr))
      (vector-ref ccla-vctr 0)
      "kanji-furi-kawari.txt"))
(define base-name (path-replace-extension infile #""))
(define out-png (path-add-extension base-name #".png"))
;;(define kiso (->ji-w-furigana "「見る^み」"));; (save-pict out-png kiso) ; ok!
(define in-lines (file->lines infile))
(define main-ln (first in-lines))
(define alt-lns (rest in-lines))
;; (define do-alt-line ;; if one section ->ji-w-furigana
;; ; if (< 1 sections) (make phrase-w-alts-pct

;;(define main-sects (get-sections main-ln))
;; whihout section-separatore 、、 
(define main-sects (string-split main-ln ruby-notation?))
(define main-pict
  (add-rectangle-background
   (apply hbl-append (map sect->pict main-sects))))
;; (save-pict out-png main-pict) ; main-pict ; ok! but, 
;; ; whay is there a frame around the kanji-furi picts?
;; 為 行為、 偽り
(define alts-pict
  (add-rectangle-background 
   (apply hb-append Kiso-alts-sep
	 (map one-sect-alt->pict alt-lns))))
;; alts-pict ; (save-pict "alt-pict-ex.png" alts-pict) ; ok!

(define Kiso-coverage-pict
  (add-rectangle-background
   (vc-append Kiso-main-alts-sep
	      main-pict 
	      alts-pict)))
Kiso-coverage-pict ; w-out lines
(save-pict "Kiso-coverage-pict-ex.png" Kiso-coverage-pict) ; ok!
;; I need syntax to name each alts pict and then pin-arrow-lines
;; from the main word to each alts.. Or I could just use length and math?? 


