
いつもつながれてほえるほかない犬です

this dog
chained always
does nothing but bark

The dog always chained
has nothing else to do but
bark bark bark bark bark

いつもつながれて
ほえるほかない
犬です

 - https://terebess.hu/english/haiku/taneda4.html
 - Kuni Shimizu 
   - http://seehaikuhere.blogspot.com/

   鉄鉢の中へ
   Hail in the Begging Bowl (鉄鉢てっぱつの中なかへも霰あられ, Teppatsu no Naka he mo Arare?) a
   鉄鉢 たっぱつ
   てっ‐ぱつ【鉄鉢】 の解説

    １ 鉄製の鉢 (はち) 。僧が托鉢 (たくはつ) で食物などを受けるのに用いる。応器。てつばち。

    ２ 兜 (かぶと) の鉢が鉄製のもの。かなばち。

 - https://www.aozora.gr.jp/index_pages/person146.html
 - http://seehaikuhere.blogspot.com/
 - https://allpoetry.com/Santoka-Taneda
 - https://terebess.hu/english/haiku/taneda.html
   - https://terebess.hu/english/haiku/taneda4.html
