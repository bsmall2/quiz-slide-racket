# quiz-slide-racket

Racket scripts for making slideshows. Experiments with text file conventions to generate slideshows.

Install racket[^1] (and maybe the text-block package too) and replace or edit the text-files for partcipatory quizzing classes.


### English Quizes

```
$ slideshow English-quiz-slides-TJ.rkt 
```

will make a slideshow quize from  a text file such as `texts/English-TakaraJima/English-Questions-1.txt with the convention of 5 line block separated by at least 2 newlines. The 5-line block must be 4 question lines and an answer line (Qs & As in same order).

The questions are inspired by an old series of Japanese books that used to introduce **Ogden's Basic English** and the **Grade Direct Method** of teaching. I made an photo album[^2] of some of the books and pages I hope to use for inspiration. For this early version of the slideshow to work the question lines must be kept brief: this limit may encourage us to avoid cliches as Orwell advises in __Politics and the English Language_. But later I might replace the 't procedure with the 'para procedure to allow for longer question sentences. 

[^2]: https://snap.as/bsmall2/quiz-ideas-for-basic-english-slideshow 

## 日本語 Japanese

### 基礎語、 土居光知の基礎日本語

土居光知の基礎語の考えをフリースフトで視覚化(一目で見るように)したら(`japanese/Kiso-Nihongo/Kotoba`):

![土居光知の言葉と関係ある18語](japanese/Kiso-Nihongo/Kotoba/Kotoba-Go-kankei-Zu-w-dot-picts.jpg "基礎語の言葉の見方")
<!-- ![土居光知の基礎語の言葉の関係ある18語のグラフ](japanese/Kiso-Nihongo/Kotoba/Kotoba-View-dot.png "基礎語の言葉のグラフ") -->
![土居光知の基礎語の言葉の関係ある18語のグラフ](japanese/Kiso-Nihongo/Kotoba/Kotoba-View-neato.png "基礎語の言葉のグラフ(graphviz neato)")

In the directory `japanese/Kiso-Nihongo/Makoto` there is a file `makoto.txt` that can be used to generate graphs:

基礎語と使用しなくても「事を足らす」言葉と一目の視覚化できます：


```
  $ racket ../Kiso-txt-\>dot-png.rkt makoto.txt
```

The same text-file and racket-script approach for visualizing the coverage of Basic (or Common 共通) words was done for other words in the directories found under `japanese/Kiso-Nihongo/`. 

![Makoto can work in place of Hontou, ShinJitsu, Jijitsu, and HonEi?](japanese/Kiso-Nihongo/Makoto/makoto.png "基礎語の誠")

Playing with graphviz's `dot` and `neato` is fun but I really want to use `racket` with `slideshow` and `pict` to make useful one-eyespan visualizations with furigana readings...

```
japanese/Kiso-Nihongo/Miru$ racket Miru-view.rkt
```

![Japanese To See (Miru) phrases to replace harder words](japanese/Kiso-Nihongo/Miru/Kisogo-Miru-w-replacements.png "見る基礎語の使い方の例としばらく不要になる言葉")


### Japanese Quizes: 日本語の能力試験の問題練習

 DrRacket(Racket)^1 をインストールしてから、まず Dai2Shu-Kanji-Oboe.rkt を<ruby><rb>使</rb><rp>「</rp><rt>つか</rt><rp>」</rp></ruby>ってみてください。 `DrRacket` で `Dai2Shu-Kanji-Oboe.rkt`を<ruby><rb>開</rb><rp>「</rp><rt>ひら</rt><rp>」</rp></ruby>いて `Run` のボタンを<ruby><rb>押</rb><rp>「</rp><rt>お</rt><rp>」</rp></ruby>すか「F5]のキーを押す。 または`quiz-slide-racket`のフォルダー(directory?)の中からコマンドラインで：
 
```
 quiz-slide-racket$ slideshow Dai2Shu-Kanji-Oboe.rkt
``` 

違う問題でスクリーンを使ってグループのクイズをしたい場合は`Dai2Shu-Kanji-Oboe.rkt`のmondai1やmondai2のテキストを訂正(編集)してください。 問題を埋めるところの前後だけのスペースが入るように、質問文を作りましゅ。 正解は、質問文の次の行のリストの一番にします。 グイズの時は正解の選択の言葉がランダムの並べ変えるので安心して、 選択の正解を並べてね。

Work on slideshow quizes from text files that obey certain conventions was inspired by a Sunday class of 'trainees' from Vietnam working on Japanese Langauge Proficiency Test questions. 


```
$ slideshow Ja-test-N3-1.rkt
```

The use of html's `ruby`-like furigana makes the text conventions more complicated.  With greater competence with regular expressions or a better understanding of `MegaParsack` the text conventions could be calmer: less like 'line-noise'... 

#### 日本語の単語や漢字の確認のためのサイト
  - English: https://en.wiktionary.org/wiki/Wiktionary:Main_Page
    - Tiếng Việtl: https://vi.wiktionary.org/wiki/%E6%BC%A2%E5%AD%97
  - 漢字：[漢字辞典」(https://kanjitisiki.com/)
    - 愛： https://kanjitisiki.com/syogako/syogaku4/001.html  	

#### 基礎日本語
- 土居光知の基礎日本語 ( _日本語の姿_ )	
    - 写真(1943年の本の頁) [アルバム](https://snap.as/bsmall2/kiso-nihongo-doi-kouchi)
    - (簡約日本語との比較) [論文](https://core.ac.uk/download/pdf/144458011.pdf)
	
### DrRacket, Racket インストール手順（Windows など）
  - [racketインストール手順（Windows）](https://learntutorials.net/ja/racket/topic/9871/%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB%E6%89%8B%E9%A0%86-windows-)
  - [初回起動時に...「DrRacketを日本語で使う」...](https://xtech.nikkei.com/atcl/nxt/column/18/02149/072600001/)
  - [Racket (DrScheme)の使用方法](http://www.stdio.h.kyoto-u.ac.jp/jugyo1/scheme/racket/Racket.html)
  
[^1]:  https://racket-lang.org/download/



